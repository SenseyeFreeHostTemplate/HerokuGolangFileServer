# Heroku Golang File Server template

##### Local run
```bash
go run web.go
```

##### Heroku
```bash
heroku apps:create golang-static-file-server
```
```bash
git push heroku master
```

##### Example:
[golang-static-file-server.herokuapp.com](https://golang-static-file-server.herokuapp.com/)

##### Auto deploy
[Deploy Go code to Heroku with GitLab CI](https://gitlab.com/sj14/go-heroku#readme)
